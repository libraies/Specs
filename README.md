# Specs #

Las especificaciones privadas de CocoaPods de Doonamis.

### Actualizar un pod ###

Se deben de seguir los siguientes pasos:

- Actualizar el código con la nueva API, correcciones, etc., que se desea tener en la siguiente versión.
- Cambiar la versión en el fichero con extensión `.podspec`.
- Hacer un commit de los cambios hechos y subirlo al repositorio.
- Validar el fichero podspec:

```shell
> pod lib lint
```

- Crear el tag de la nueva versión y subirla al repositorio:

```shell
> git tag 1.0.1
> git push --tags
```

- Validar que la configuración entre el repositorio con el código fuente y el fichero podspec es correcta:

```shell
> pod spec lint
```

- Crear la nueva versión del pod en el repositorio:

```shell
pod repo push doonamis <PODSPEC_FILE_NAME>.podspec
```

El siguiente paso es cambiar la versión en el fichero Podfile del proyecto a la nueva versión que te deseas descargar y actualizar el pod.