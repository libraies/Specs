Pod::Spec.new do |s|
  s.name             = "AdaptedUI"
  s.version          = "1.0.7"
  s.summary          = "A collection of classes that allows you to change sizes according to the screen size."
  s.homepage         = "https://bitbucket.org/doonamis/adaptedui"
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { "Doonamis" => "lcardenas@doonamis.es" }
  s.source           = { :git => "https://bitbucket.org/doonamis/adaptedui.git", :tag => s.version }

  s.platform     = :ios, '9.0'
  s.source_files = 'Source/**/*.swift'
  s.requires_arc = true
  s.module_name = 'AdaptedUI'
end
